// ========== IMPORTACIONES DE LIBRERIAS ==========
// se importa la libreria react
import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

// ========== IMPORTACIONES DE ESTILOS ==========
// se importan los estilos generales de todo el aplicativo de App.css
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// ========== IMPORTACIONES DE COMPONENTES "PADRES" ==========
// se importa el componente padre del header
import HeaderJack from "./components/header";
import FooterJack from "./components/footer";
import User from "./components/users";
import Company from "./components/companies";

// ========== IMPORTACION DE IMAGENES ==========
// se importan las imagenes usadas
import logo from './logo.svg';

// ========== IMPORTACIONES DE OTROS ==========

function App() {

  const animal = {
    name: 'Halcon peregrino',
    slogan: 'Rapidos y veloz'
  }

  return (
    <div className="App">
      <header className="App-header">
        <Router>
          <Link to="/" className="btn btn-warning">Inicio</Link>
          <Link to="/users" className="btn btn-warning">Usuarios</Link>
          <Link to="/footer" className="btn btn-warning">Footer</Link>
          <Link to="/header" className="btn btn-warning">Header</Link>

          <Switch>
            <Route exac path="/users" component={User}/>
            <Route path="/footer" exac>
              <FooterJack animal={animal}/>
            </Route>
            <Route exac path="/header">
              <HeaderJack/>
            </Route>
            <Route exac path="/">
              Inicio
              <Company 
                titlePrincipal="Hola props de clases"
                secondItem="texto lorem ipsum"
              />
            </Route>
          </Switch>
        </Router>
        <img src={logo} className="App-logo" alt="logo" />
        
        <p>Hola react js</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        
      </header>
    </div>
  );
}

export default App;
