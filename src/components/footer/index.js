// se importa la libreria de react
import React from "react";
import { Carousel } from 'react-bootstrap';

// ========== IMPORTACION DE IMAGENES ==========
// se importan las imagenes usadas en el componente footer
import gato from './images/gatito.jpg';
import perro from './images/perrito.jpg';

// se genera nuestro componente funcional
function Footer({animal}){
  return(
    <div>
      <h1>Hola soy el footer</h1>
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://i1.wp.com/www.halconerosdecastilla.com/blog/wp-content/uploads/2018/04/halcon-peregrino-caza.jpg?fit=980%2C550&ssl=1"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>{animal.name}</h3>
            <p>{animal.slogan}</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://vignette.wikia.nocookie.net/reinoanimalia/images/a/ac/Leon_macho.png/revision/latest?cb=20130413133154&path-prefix=es"
            alt="Third slide"
          />

          <Carousel.Caption>
            <h3>dsfgh</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={gato}
            alt="Third slide"
          />

          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={perro}
            alt="Third slide"
          />

          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
};

export default Footer;