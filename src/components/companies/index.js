import React, {Fragment} from "react";

class Company extends React.Component{
  render(){
    return(
      <Fragment>
        <div className="jumbotron">
    <h1 className="display-3">{this.props.titlePrincipal}</h1>
          <hr className="my-4"></hr>
          <p>{this.props.secondItem}</p>
          <p className="lead">
            <a className="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p>
        </div>
      </Fragment>  
    )
  }
}

export default Company;